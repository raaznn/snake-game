import pygame
import random
pygame.init()
win = pygame.display.set_mode((600,600))
audio = pygame.mixer.Sound('audio.wav')

x = 20
y = 20
direction =0
s_size = 1
snake_x = [x]
snake_y = [y]
s_radius =10
delay = 100


food_x = random.randint(1,29) * 20
food_y = random.randint(1,29) * 20 


def crawl (d):
	global x
	global y
	global snake_x
	global snake_y
	
	if d==0:
		x=(x+20)%600
	if d==1:
		y=(y+20)%600		
	if d==2:
		x=(x-20)%600
	if d==3:
		y=(y-20)%600

	if x == 0 and d == 0:
		x = 20
	if y == 0 and d == 1:
		y = 20
	if x == 0 and d == 2:
		x = 580
	if y == 0 and d == 3:
		y = 580

	snake_x.insert(0,x)
	snake_y.insert(0,y)

	while (len(snake_x) > s_size) and (len(snake_y)> s_size): 
		snake_x.pop()
		snake_y.pop()

def drawsnake ():
	win.fill((0,0,0))

	for n in range (0,s_size):
		pygame.draw.circle(win,(200,200,0),(snake_x[n],snake_y[n]),s_radius)
	
	drawfood()

	pygame.display.update()
	pygame.time.delay(delay)

def drawfood():
	pygame.draw.circle(win,(255,20,20),(food_x,food_y),s_radius)

def new_food():
	global food_x
	global food_y
	global delay
	food_x = random.randint(1,29) * 20
	food_y = random.randint(1,29) * 20 
	delay -=5

def check_collision():
	pos_x = snake_x[0]
	pos_y = snake_y[0]
	global s_size,delay

	for n in range(1,s_size):
		
		if pos_x == snake_x[n] and pos_y== snake_y[n]:
			pygame.time.delay(1500) 
			s_size = 1
			delay = 100
			
def getdirection():
	global direction
	keys = pygame.key.get_pressed()
	if keys[pygame.K_LEFT]:
		direction = 2
	if keys[pygame.K_RIGHT]:
		direction = 0
	if keys[pygame.K_UP]:
		direction = 3
	if keys[pygame.K_DOWN]:
		direction = 1


def food():

	if food_x==snake_x[0] and food_y == snake_y[0]:
		pygame.mixer.Sound.play(audio)
		return True
	return False


def snakegrow():

	global s_size
	s_size += 1

def main():

	run = True

	while run:
		for e in pygame.event.get():
			if e.type == pygame.QUIT:
				run = False
		

		getdirection()

		crawl(direction)
		
		drawsnake()
		
		check_collision()
		
		if food():
			snakegrow()
			new_food()


if __name__ == "__main__":	
	main()

